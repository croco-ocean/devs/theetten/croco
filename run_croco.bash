#!/bin/bash

export MY_CONFIG="BOB2500"

export MY_CONFIG_NAME="BOB2500_EXP2"

export INPUT_FILES_DIR="./CROCO_FILES"

# create SCRATCH directory for execution
export TMP_DIR=${SCRATCHDIR}/TMP_${MY_CONFIG_NAME}
mkdir ${TMP_DIR}

# create directories on TMP_DIR
export TMP_INPUT_FILES_DIR=${TMP_DIR}/CROCO_FILES
mkdir ${TMP_INPUT_FILES_DIR}

# copy executable
if [ -e croco ]
then
   cp croco ${TMP_DIR}
else
   echo ".............................."
   echo " ...executable not compiled..."
   echo ".............................."
   exit
fi

# copy xios_server
cp xios_server.exe ${TMP_DIR}
cp xios_server.conf ${TMP_DIR}

# copy input files
cp croco.in_template ${TMP_DIR}
cp *.xml ${TMP_DIR}
#cp ${INPUT_FILES_DIR}/croco_grd.nc ${TMP_INPUT_FILES_DIR}

# get scrum_time for croco_rst or croco_ini file
cp what_time.?? ${TMP_DIR}

cd ${TMP_INPUT_FILES_DIR}

# copy or link input data files :
cp ../../${MY_CONFIG}/INI/* .
ln -s ../../${MY_CONFIG}/GRD/* .
ln -s ../../${MY_CONFIG}/BRY/* .
ln -s ../../${MY_CONFIG}/RUNOFF/* .
#for VARIABLE in  Downward_Long-Wave_Rad_Flux  Downward_Short-Wave_Rad_Flux_surface  Precipitation_rate Specific_humidity Temperature_height_above_ground U-component_of_wind Upward_Long-Wave_Rad_Flux_surface Upward_Short-Wave_Rad_Flux_surface V-component_of_wind ; do
#   for YEAR in $(seq 2007 2010) ; do
#       for MONTH in $(seq 01 12) ; do
#          ln -s ../../CFSR/${VARIABLE}_Y${YEAR}.nc ${VARIABLE}_Y${YEAR}M${MONTH}.nc
#       done
#   done
#done
for variable in Q SSR STRD T2M TP U10M V10M ; do
   ln -s /store/gcharriagd/ERAI/${VARIABLE}* .
done


cd ${TMP_DIR}

chmod 755 what_time.??
rm -f scrum_time.file day.file croco.in
./what_time.sh>scrum_time.file
year=`cut --delimiter='-' -f1 scrum_time.file`
month=`cut --delimiter='-' -f2 scrum_time.file`; month=$(printf "%02d" $month)
cut --delimiter='-' -f3 scrum_time.file > day.file ; day=`cut --delimiter=' ' -f1 day.file` ; day=$(printf "%02d" $day)
sed  -e "s/DAY/${day}/" -e "s/MONTH/${month}/" -e "s/YEAR/${year}/" croco.in_template > croco.in

cd ${HOME}/Croco-git/CONFIGS/${MY_CONFIG_NAME}

pwd 

output_sbatch=$(sbatch run_croco.slurm)
echo $output_sbatch
jobid_1=${output_sbatch#*"job "}
export JOB_ID=$jobid_1
jobid_2=$(sbatch --dependency=afterany:$jobid_1 rapatrie.slurm)
echo $jobid_2
