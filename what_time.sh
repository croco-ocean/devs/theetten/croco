#!/bin/bash 

set +x 

savepath=$PATH
export PATH=/opt/software/tools/anaconda2/bin:$PATH
source activate  uvcdat_2.6.1

time_units='seconds since 1900-1-1'

init_file=`ls -1 ./CROCO_FILES/croco_ini*|head -1`
if [ $init_file != " " ] ; then
  val=`ncdump -v scrum_time ${init_file} |grep "scrum_time ="|cut -c14-25`
fi

toto=`./what_time.py $val`
echo $toto

source deactivate  uvcdat_2.6.1
export PATH=$savepath
