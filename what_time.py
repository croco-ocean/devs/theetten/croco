#! /usr/bin/env python
# -*- coding: utf-8 -*-
# _____________________________________________________________________________
#
# Convert relative time in seconds or days since a date in a "readable" time.
#
# Created: 08/2015 (G. Charria)
# _____________________________________________________________________________
import cdtime
from time import strftime
from optparse import OptionParser
import sys, os, shutil
parser = OptionParser(usage="Usage: %prog [options] datetoconvert",
    description="Show date from a relative time.")
(options, args) = parser.parse_args()
val = float(args[0])
time_units = 'seconds since 1900-1-1'
time=cdtime.reltime(val,time_units).tocomp()
print time
