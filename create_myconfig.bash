#!/bin/bash
#
# G. Cambon : Sept. 2016
# S. Theetten : April 2019
#
#
clear
set +x
#==========================================================================================
# BEGIN USER SECTION
#
SOURCES_DIR='../../croco'
MY_SOURCES_DIR='../../theetten'
#TOOLS_DIR='../../croco_tools'

MY_CONFIG_PATH='../CONFIGS/'
MY_CONFIG_NAME=$1

#
# END USER SECTION
#==========================================================================================
if [ $1 == "" ]; then
echo "Use : ./mkXXX_config MY_CONFIG_NAME"
exit 1
fi

while getopts "h" V
do
  case $V in
    h) echo "";
    echo " Script to setup your own croco configuration.";
    echo " ";
    echo " Use : ./create_myconfig.bash MY_CONFIG_NAME";
    echo " What is does :";
    echo " - Copy the original croco/Run with cppdefs.h, param.h and *.in files needed";
    echo "";
    echo "Edit the USER SECTION of the script to define the following variables :";
    echo " - SOURCES_DIR     : location of croco directory";
    echo " - MY_CONFIG_PATH  : location of the repository to store the configuration";
    echo " - MY_CONFIG_NAME  : name of the configuration ";    
    exit 0;;
  esac
done

## Check if source are there
#if [ ! -d $SOURCES_DIR ]; then 
#	echo 'Directory for croco not found ...'
#	echo 'Check the SOURCES_DIR variable ...'
#	echo 'Exiting ...'
#   exit 1
#fi

 
# Create the directory
if [ ! -d $MY_CONFIG_PATH ]; then 
    mkdir $MY_CONFIG_PATH
    if [ ! -d $MY_CONFIG_PATH'/'$MY_CONFIG_NAME ]; then
	mkdir $MY_CONFIG_PATH'/'$MY_CONFIG_NAME
	copy_tag=1
    else
	echo 'Already a configuration exists ...'
	echo 'Check the configuration directory ' $MY_CONFIG_PATH'/'$MY_CONFIG_NAM
	copy_tag=0
    fi
else
    if [ ! -d $MY_CONFIG_PATH'/'$MY_CONFIG_NAME ]; then
	mkdir $MY_CONFIG_PATH'/'$MY_CONFIG_NAME
	copy_tag=1
    else
	echo 'Already a configuration exists ...'
	echo 'Check the configuration directory ' $MY_CONFIG_PATH'/'$MY_CONFIG_NAM
	copy_tag=0
    fi
fi
TMP_DIR=${SCRATCHDIR}'/TMP_'${MY_CONFIG_NAME}
if [ ! -d $TMP_DIR ]; then
    mkdir $TMP_DIR
    if [ ! -d $TMP_DIR'/CROCO_FILES' ]; then
        mkdir $TMP_DIR'/CROCO_FILES'
    fi
fi



if [[ $copy_tag == 1 ]] ; then
    
    cd $MY_CONFIG_PATH'/'$MY_CONFIG_NAME
    
    #OCEAN
    DIRO='OCEAN'
    cp -Rf $MY_SOURCES_DIR/$DIRO/* .
   
    # XIOS
    DIRO='XIOS'
    cp -Rf $MY_SOURCES_DIR/$DIRO/*.F .
    cp -Rf $MY_SOURCES_DIR/$DIRO/domain_def.xml .
    cp -Rf $MY_SOURCES_DIR/$DIRO/field_def.xml_full . 
    cp -Rf $MY_SOURCES_DIR/$DIRO/iodef.xml . 
#    cp -Rf $SOURCES_DIR/$DIRO/xios_launch.file .
#    cp -Rf $SOURCES_DIR/$DIRO/README_XIOS .

    # TEST_CASE + NAMELIST_OANALYSIS
    cp -Rf $MY_SOURCES_DIR/xios_server.conf .
    cp -Rf $MY_SOURCES_DIR/*_template .
    rm -f croco.in
    cp -Rf $MY_SOURCES_DIR/what_time* .
    cp -Rf $MY_SOURCES_DIR/*bash .
    
    echo '         '
    echo '=> Copy modified files from' ; realpath $MY_SOURCES_DIR 
    echo '         '
#    # Link the files from croco_tools/
#    # for crocotools in matlab
#    cp -Rf $TOOLS_DIR/start.m .
#    cp -Rf $TOOLS_DIR/crocotools_param.m .
#    #cp -Rf $TOOLS_DIR/Misc/town.dat Misc/
#    #cp -Rf $TOOLS_DIR/Town/town.dat Misc/
    #

    cp -Rf $MY_SOURCES_DIR/create_myconfig.bash create_myconfig.bash.BCK
    #
#    echo '=> Copy from '$TOOLS_DIR ' done'
    
fi

#cd $MY_CONFIG_PATH'/'$MY_CONFIG_NAME

echo ''
echo '   needed to setup your own simulations in ' $PWD
echo '         '

sed  -e "s/AAAAAAAA/${MY_CONFIG_NAME}/" run_croco.slurm_template > run_croco.slurm
sed  -e "s/AAAAAAAA/${MY_CONFIG_NAME}/" rapatrie.slurm_template > rapatrie.slurm
